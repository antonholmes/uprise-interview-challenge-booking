import React, { useState, useEffect } from 'react'
import BookingForm from './BookingForm'
import firebaseDb from '../firebase'

const Bookings = () => {

  var [bookingObjects, setBookingObjects] = useState({})
  var [currentId, setCurrentId] = useState()

  useEffect(() => {
    firebaseDb.child('bookings').on('value', snapshot => {
      if (snapshot.val() != null)
        setBookingObjects({
          ...snapshot.val()
        })
      else
        setBookingObjects({})
    })
  }, [])

  const addOrEdit = obj => {
    if (currentId == '')
      firebaseDb.child('bookings').push(
        obj,
        err => {
          if (err)
            console.log(err)
          else
            setCurrentId('')
        }
      )
    else
      firebaseDb.child(`bookings/${currentId}`).set(
        obj,
        err => {
          if (err)
            console.log(err)
          else
            setCurrentId('')
        }
      )
  }

  const onDelete = key => {
    if (window.confirm('Are you sure you want to delete this record?')) {
      firebaseDb.child(`bookings/${key}`).remove(
        err => {
          if (err)
            console.log(err)
          else
            setCurrentId('')
        }
      )
    }
  }
  return (
    <React.Fragment>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4" text-center>Book a Call</h1>
        </div>
      </div>
      <div className='row'>
        <div className='col-md-5'>
          <BookingForm {...({ addOrEdit, currentId, bookingObjects })} />
        </div>
        <div className='col-md-7'>
          <table className='table table-borderless table-stripped'>
            <thead className='thead-light'>
              <tr>
                <th>Full Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {
                Object.keys(bookingObjects).map(id => {
                  return <tr key={id}>
                    <td>{bookingObjects[id].fullName}</td>
                    <td>{bookingObjects[id].mobile}</td>
                    <td>{bookingObjects[id].email}</td>
                    <td>
                      <a className='btn text-primary' onClick={() => { setCurrentId(id) }}>
                        <i className='fas fa-pencil-alt'></i>
                      </a>
                      <a className='btn text-danger' onClick={() => onDelete(id)}>
                        <i className='far fa-trash-alt'></i>
                      </a>
                    </td>
                  </tr>
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Bookings
