import * as firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyBuUbYzWq_Q79KiomE9nUoEMiiANawPBLk",
  authDomain: "react-crud-27993.firebaseapp.com",
  databaseURL: "https://react-crud-27993.firebaseio.com",
  projectId: "react-crud-27993",
  storageBucket: "react-crud-27993.appspot.com",
  messagingSenderId: "622817004106",
  appId: "1:622817004106:web:c01a224a8e7d873a6c74b3"
};
// Initialize Firebase
var fireDb = firebase.initializeApp(firebaseConfig);

export default fireDb.database().ref()
