import React from 'react';
import logo from './logo.svg';
import './App.css';
import Bookings from './components/Bookings';

function App() {
  return (
    <div className='row'>
      <div className='col-md-8 offset-md-2'>
        <Bookings />
      </div>
    </div>
  );
}

export default App;
